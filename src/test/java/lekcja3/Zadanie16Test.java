package lekcja3;

import org.junit.Test;

import static org.junit.Assert.*;

public class Zadanie16Test {

    @Test
    public void one() {
        String[] args = {"2", "5", "-2"};

        assertEquals(-20.0, Zadanie16.multiply(args), 0.0);
    }

    @Test
    public void other() {
        String[] args = {"-0.5", "5.123", "-2.1"};

        assertEquals(5.37915, Zadanie16.multiply(args), 0.0);
    }
}
