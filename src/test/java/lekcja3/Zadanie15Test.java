package lekcja3;

import org.junit.Test;

import static org.junit.Assert.*;

public class Zadanie15Test {

    @Test
    public void calculate() {
        String[] partsOfPi = {"1", "2.14", "0.00159265359"};
        assertEquals(3.14159265359, Zadanie15.sum(partsOfPi), 0.0);
    }
}
