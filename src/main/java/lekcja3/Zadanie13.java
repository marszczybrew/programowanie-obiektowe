package lekcja3;

public class Zadanie13 {

    public static void main(String[] args) {
        try {
            String message = "Result of adding " + args[0] + " and " + args[1] + " is " + sum(args[0], args[1]);

            System.out.println(message);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input, int is required");
            System.exit(1);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Two integers are required");
            System.exit(1);
        }
    }

    static int sum(String one, String another) {
        return Integer.parseInt(one) + Integer.parseInt(another);
    }
}
