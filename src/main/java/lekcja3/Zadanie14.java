package lekcja3;

public class Zadanie14 {

    public static void main(String[] args) {
        try {
            String message = "Result of multiplying " + args[0] + " by " + args[1] + " is " + multiply(args[0], args[1]);

            System.out.println(message);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input, int is required");
            System.exit(1);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Two integers are required");
            System.exit(1);
        }
    }

    static int multiply(String one, String another) {
        return Integer.parseInt(one) + Integer.parseInt(another);
    }
}
