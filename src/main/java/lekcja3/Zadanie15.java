package lekcja3;

import java.util.*;

public class Zadanie15 {
    public static void main(String[] numbers) {
        try {
            // parse arguments into floats, sum them afterwards
            double sum = sum(numbers);
            // concatenate all arguments into the message, append sum
            String message = "Result of adding " + String.join(" and ", numbers) + " is " + sum;

            System.out.println(message);
        } catch (NumberFormatException e) {
            // if some argument wasn't a number
            System.out.println("Invalid input, numbers are required");
            System.exit(1);
        } catch (NoSuchElementException e) {
            // if there were no arguments
            System.out.println("You need to pass some numbers as parameters");
            System.exit(1);
        }
    }

    static double sum(String[] args) {
        return Arrays.stream(args).map(Double::parseDouble).reduce((x, y) -> x + y).get();
    }
}
