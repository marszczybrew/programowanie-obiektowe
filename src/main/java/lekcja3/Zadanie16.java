package lekcja3;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Zadanie16 {
    public static void main(String[] numbers) {
        try {
            // parse arguments into floats, sum them afterwards
            double result = multiply(numbers);
            // concatenate all arguments into the message, append sum
            String message = "Result of multiplying " + String.join(" by ", numbers) + " is " + result;

            System.out.println(message);
        } catch (NumberFormatException e) {
            // if some argument wasn't a number
            System.out.println("Invalid input, numbers are required");
            System.exit(1);
        } catch (NoSuchElementException e) {
            // if there were no arguments
            System.out.println("You need to pass some numbers as parameters");
            System.exit(1);
        }
    }

    static Double multiply(String[] args) {
        return Arrays.stream(args).map(Double::parseDouble).reduce((x, y) -> x * y).get();
    }
}
